module ua.com.sut.kursova {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires java.desktop;

    opens ua.com.sut.kursova to javafx.fxml;
    exports ua.com.sut.binarytree;
}