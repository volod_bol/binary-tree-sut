package ua.com.sut.binarytree;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class BinaryTreeApplication extends Application {

    int inSize = 0;
    int a = 0;

    @Override
    public void start(Stage primaryStage) {
        BinaryTree<Integer> tree = new BinaryTree<>();

        BorderPane pane = new BorderPane();
        TreePrinter view = new TreePrinter(tree);
        pane.setCenter(view);

        TextField tfKey = new TextField();
        tfKey.setPrefColumnCount(3);
        tfKey.setAlignment(Pos.BASELINE_RIGHT);
        Button btInsert = new Button("Insert");
        Button btDelete = new Button("Delete");
        Button inorder = new Button("inorder");
        HBox hBox = new HBox(5);
        hBox.getChildren().addAll(new Label("Enter a key: "),

                tfKey, btInsert, btDelete, inorder);
        hBox.setAlignment(Pos.CENTER);
        pane.setBottom(hBox);
        ArrayList<Integer> arrayIno = new ArrayList<>();
        Collections.sort(arrayIno);
        arrayIno.add(10);
        arrayIno.add(15);
        arrayIno.add(20);
        arrayIno.add(25);
        btInsert.setOnAction(e -> {
            inSize = 10000;
            int key = Integer.parseInt(tfKey.getText());
            if (tree.search(key)) {
                view.displayTree();
                view.setStatus(key + " is already in the tree");

            } else {
                arrayIno.add(key);
                tree.insert(key);
                view.displayTree();
                view.setStatus(key + " is inserted in the tree");

            }
            Collections.sort(arrayIno);

        });

        btDelete.setOnAction(e -> {
            int key = Integer.parseInt(tfKey.getText());
            inSize = 10000;
            if (!tree.search(key)) {
                view.displayTree();
                view.setStatus(key + " is not in the tree");
            } else {

                arrayIno.removeIf(integer -> integer == key);

                tree.delete(key);
                view.displayTree();
                view.setStatus(key + " is deleted from the tree");

            }

        });

        inorder.setOnAction(e -> {
            inSize = 0;
            view.displayTree();
            view.setStatus("Travel tree is [inorder]" + arrayIno);
            Map<Integer, Circle> map = view.getMap();
            for (Integer i : arrayIno) {
                Circle circle4 = map.get(i);
                circle4.setFill(Color.GRAY);

            }
            Timer timer;

            timer = new Timer(1000, e1 -> {
                for (Integer i : arrayIno) {
                    Circle circle5 = map.get(i);
                    circle5.setStrokeWidth(1);
                    circle5.setStroke(Color.GRAY);
                }

                a++;
                if (inSize < arrayIno.size()) {
                    Circle circle = map.get(arrayIno.get(inSize));
                    circle.setStrokeWidth(3);
                    circle.setFill(Color.rgb(255, 15, 50));

                    circle.setStroke(Color.RED);

                    inSize = inSize + 1;

                } else {
                    if (inSize == arrayIno.size()) {
                        for (Integer i : arrayIno) {
                            Circle circle2 = map.get(i);
                            circle2.setStrokeWidth(3);

                        }
                        inSize = inSize + 1;
                    } else {

                        for (Integer i : arrayIno) {
                            Circle circle3 = map.get(i);
                            circle3.setFill(Color.GRAY);

                        }
                    }
                }

            });

            timer.start();
            if (a > 1) {
                timer.stop();
            }
            inSize = 0;
        });

        Scene scene = new Scene(pane, 500, 300);
        primaryStage.setTitle("Name Surname KND-21");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();

        tree.insert(20);
        tree.insert(15);
        tree.insert(10);
        tree.insert(25);
        view.displayTree();
    }

}