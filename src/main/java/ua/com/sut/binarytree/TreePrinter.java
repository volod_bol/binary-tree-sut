package ua.com.sut.binarytree;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import java.util.HashMap;
import java.util.Map;

public class TreePrinter extends Pane {
    private static final double V_GAP = 50;

    private final BinaryTree<Integer> tree;
    private final Map<Integer, Circle> map = new HashMap<>();

    TreePrinter(BinaryTree<Integer> tree) {
        this.tree = tree;
        setStatus("Tree is empty");
    }

    public Map<Integer, Circle> getMap() {
        return this.map;
    }

    public void setStatus(String msg) {
        getChildren().add(new Text(20, 20, msg));
    }

    public void displayTree() {
        this.getChildren().clear();
        if (tree.getRoot() != null) {
            displayTree(tree.getRoot(), getWidth() / 2, V_GAP, getWidth() / 4);
        }
    }

    private void displayTree(BinaryTree.TreeNode<Integer> root, double x, double y, double hGap) {
        if (root.getLeft() != null) {
            getChildren().add(new Line(x - hGap, y + V_GAP, x, y));
            displayTree(root.getLeft(), x - hGap, y + V_GAP, hGap / 2);
        }

        if (root.getRight() != null) {
            getChildren().add(new Line(x + hGap, y + V_GAP, x, y));
            displayTree(root.getRight(), x + hGap, y + V_GAP, hGap / 2);
        }

        double radius = 15;
        Circle circle = new Circle(x, y, radius);
        circle.setFill(Color.GRAY);
        circle.setStroke(Color.GRAY);

        map.put(root.getElem(), circle);

        getChildren().addAll(circle, new Text(x - 4, y + 4, root.getElem() + ""));

    }
}