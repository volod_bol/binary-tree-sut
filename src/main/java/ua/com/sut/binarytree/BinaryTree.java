package ua.com.sut.binarytree;

public class BinaryTree<E extends Comparable<E>> {

    private TreeNode<E> root;

    public boolean search(E e) {
        TreeNode<E> current = root;
        while (current != null) {
            if (e.compareTo(current.getElem()) < 0) {
                current = current.getLeft();
            } else if (e.compareTo(current.getElem()) > 0) {
                current = current.getRight();
            } else
                return true;
        }
        return false;
    }

    public void insert(E e) {
        if (root == null)
            root = createNewNode(e);
        else {
            TreeNode<E> parent = null;
            TreeNode<E> current = root;
            while (current != null)
                if (e.compareTo(current.getElem()) < 0) {
                    parent = current;
                    current = current.getLeft();
                } else if (e.compareTo(current.getElem()) > 0) {
                    parent = current;
                    current = current.getRight();
                }
            if (e.compareTo(parent.getElem()) < 0) {
                parent.setLeft(createNewNode(e));
            } else {
                parent.setRight(createNewNode(e));
            }
        }
    }

    protected TreeNode<E> createNewNode(E e) {
        return new TreeNode<>(e);
    }

    protected void preorder(TreeNode<E> root) {
        if (root == null)
            return;
        postorder(root.getLeft());

        postorder(root.getRight());

    }

    protected void postorder(TreeNode<E> root) {
        if (root == null)
            return;

        preorder(root.getLeft());

        preorder(root.getRight());
    }

    public static class TreeNode<E extends Comparable<E>> {
        private E elem;
        private TreeNode<E> left;

        private TreeNode<E> right;

        public TreeNode(E e) {
            setElem(e);
        }

        public TreeNode<E> getLeft() {
            return left;
        }

        public void setLeft(TreeNode<E> left) {
            this.left = left;
        }

        public TreeNode<E> getRight() {
            return right;
        }

        public void setRight(TreeNode<E> right) {
            this.right = right;
        }

        public E getElem() {
            return elem;
        }

        public void setElem(E element) {
            this.elem = element;
        }
    }

    public TreeNode<E> getRoot() {
        return root;
    }

    public void delete(E e) {
        TreeNode<E> parent = null;
        TreeNode<E> current = root;
        while (current != null) {
            if (e.compareTo(current.getElem()) < 0) {
                parent = current;
                current = current.getLeft();
            } else if (e.compareTo(current.getElem()) > 0) {
                parent = current;
                current = current.getRight();
            } else {
                break;
            }
        }
        if (current.getLeft() == null) {
            if (parent == null) {
                root = current.getRight();
            } else {
                if (e.compareTo(parent.getElem()) < 0) {
                    parent.setLeft(current.getRight());
                } else {
                    parent.setRight(current.getRight());
                }
            }
        } else {
            TreeNode<E> parentOfRightMost = current;
            TreeNode<E> rightMost = current.getLeft();
            while (rightMost.getRight() != null) {
                parentOfRightMost = rightMost;
                rightMost = rightMost.getRight();
            }
            current.setElem(rightMost.getElem());
            if (parentOfRightMost.getRight() == rightMost) {
                parentOfRightMost.setRight(rightMost.getLeft());
            } else {
                parentOfRightMost.setLeft(rightMost.getLeft());
            }
        }
    }

}
